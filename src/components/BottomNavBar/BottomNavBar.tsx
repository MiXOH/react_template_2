import React from "react";
import { Button, AppBar, Toolbar } from "@material-ui/core";

import styles from "./BottomNavBar.module.scss";

export interface BottomNavBarItems {
    title: string;
    link: string;
}

export interface BottomNavBarProps {
    item: BottomNavBarItems
}

export class BottomNavBar extends React.PureComponent<BottomNavBarProps> {
    constructor(props: BottomNavBarProps) {
        super(props);
    }
    public render() {
        const { item } = this.props;
        return (
            <AppBar position="fixed" color="primary" className={styles.appBar}>
                <Toolbar className={styles.toolbarContainer}>
                    <div>
                        <Button color="inherit" href={item.link}>
                            {item.title}
                        </Button>
                    </div>

                    <div>
                        <Button color="inherit" href={"/"}>
                            Rules
                        </Button>
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}