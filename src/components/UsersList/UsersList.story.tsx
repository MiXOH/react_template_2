import { storiesOf } from "@storybook/react";
import { UsersList } from "./UsersList";
import React from "react";
import { action } from "@storybook/addon-actions";
import { UserRole } from "../../ducks/users/users";

storiesOf("UsersList", module).add("default", () => {
    const users = [
        { id: "1", email: "Wtf2019", role: UserRole.ROLE_CLIENT, password: "Master" },
        {
            id: "1",
            email: "Wtf2019",
            role: UserRole.ROLE_PROVIDER,
            password: "Master",
        },
    ];

    const onEdit = action("onEdit");
    const onDelete = action("onDelete");
    const onQuery = action("onQuery");

    return <UsersList users={users} onEdit={onEdit} onDelete={onDelete} onQuery={onQuery} />;
});
