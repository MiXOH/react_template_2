import React from 'react';

import styles from "./UserMoneyCount.module.scss";

export interface UserMoneyCountStateProps {
    countMoney: number | null | undefined,
}

export type UserMoneyCountProps = UserMoneyCountStateProps;

export class UserMoneyCount extends React.PureComponent<UserMoneyCountProps> {
    constructor(props: UserMoneyCountProps) {
        super(props);
    }

    public render() {
        const classes = styles;

        const { countMoney } = this.props;

        return (
            <span>
                Денег {countMoney}
            </span>
        );
    }
}