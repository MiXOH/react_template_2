import { connect, MapDispatchToProps, MapStateToProps } from "react-redux";
import { UserMoneyCount, UserMoneyCountStateProps } from "./UserMoneyCount";
import { State } from "../../ducks/rootReducer";
import { selectUsersCountMoney } from "../../selectors/selectors";

const mapStateToProps: MapStateToProps<UserMoneyCountStateProps, {}, State> = state => {
    return {
        countMoney: selectUsersCountMoney(state),
    }
}

export const UserMoneyCountConnected = connect(
    mapStateToProps,
    null,
)(UserMoneyCount);
