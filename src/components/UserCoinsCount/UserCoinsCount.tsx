import React from 'react';

import styles from "./UserCoinsCount.module.scss";

export interface UserCoinsCountStateProps {
    countCoins: number | null | undefined,
}

export type UserCoinsCountProps = UserCoinsCountStateProps;

export class UserCoinsCount extends React.PureComponent<UserCoinsCountProps> {
    constructor(props: UserCoinsCountProps) {
        super(props);
    }

    public render() {
        const classes = styles;

        const { countCoins } = this.props;

        return (
            <span>
                Монет {countCoins}
            </span>
        );
    }
}