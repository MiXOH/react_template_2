import { connect, MapDispatchToProps, MapStateToProps } from "react-redux";
import { UserCoinsCount, UserCoinsCountStateProps } from "./UserCoinsCount";
import { State } from "../../ducks/rootReducer";
import { selectUsersCountCoins } from "../../selectors/selectors";

const mapStateToProps: MapStateToProps<UserCoinsCountStateProps, {}, State> = state => {
    return {
        countCoins: selectUsersCountCoins(state),
    }
}

export const UserCoinsCountConnected = connect(
    mapStateToProps,
    null,
)(UserCoinsCount);
