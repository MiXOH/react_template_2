/**
 * План:
 * + 1. Сделать нужную визуализацию, чтобы работало как надо
 * + 2. Составить схему state для компонента
 * + 3. Перевести компонент на классы
 * + 4. Внедрить компонент в общий state приложения
 * 5. Подключить необходимые методы backend-а
 * 5.1. Определить нужные методы
 * 5.2. Подключить методы к компоненту
 */


import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import Button from '@material-ui/core/Button';

import styles from "./CoinsBuy.module.scss";

import { CoinsCountConnected } from "../CoinsCount/CoinsCountConnected";
import {Platforms} from "../../ducks/platform/platform";

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

function a11yProps(index: any) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

interface CoinsBuyState {
    currentTab: number,
}

export interface CoinsBuyDispatchProps {
    setPlatform: (platform: Platforms) => void,
    beginOrderAndGoToRegistry: () => void
}

export interface CoinsBuyStateProps {
    countCoins: number,
    platform: Platforms | undefined,
    isAuth: boolean,
    isOrderInProgress: boolean,
}

interface MyTab {
    label: string,
    index: number,
    disabled: boolean,
}

export type CoinsBuyProps = CoinsBuyStateProps & CoinsBuyDispatchProps;

export class CoinsBuy extends React.PureComponent<CoinsBuyProps, CoinsBuyState> {
    constructor(props: CoinsBuyProps) {
        super(props);

        const { isOrderInProgress } = props;
        let currentTab = 0;
        if (isOrderInProgress) {
            currentTab = 3;
        }
        this.state = {
            currentTab: currentTab,
        }
    }

    getMyTabList(): MyTab[] {
        const { currentTab } = this.state;
        const { platform, countCoins } = this.props;
        let myTabList: MyTab[];
        myTabList = [
            {
                label: "1 Платформа " + String(platform),
                index: 0,
                disabled: false,
            },
            {
                label: "2 Количество " + String(countCoins),
                index: 1,
                disabled: true,
            },
            {
                label: "3 Оплата",
                index: 2,
                disabled: true,
            },
            {
                label: "4 Доставка",
                index: 3,
                disabled: true,
            }
        ];

        myTabList.forEach(function(item, i, arr) {
            if (i > currentTab) {
                item.disabled = true;
            }
            else {
                item.disabled = false;
            }
        });

        return myTabList;
    }

    setCurrentTab = (newTab: number) => {
        this.setState({ currentTab: newTab });
    }

    setPlatform = (curPlatform: Platforms) => {
        this.props.setPlatform(curPlatform);
    }

    handleChange = (event: React.ChangeEvent<{}>, newTab: number) => {
        this.setCurrentTab(newTab);
    }

    setPlayStation = () => {
        this.setCountCoins(Platforms.PLATFORM_PLAY_STATION_4);
    }

    setXbox = () => {
        this.setCountCoins(Platforms.PLATFORM_XBOX_ONE);
    }

    setCountCoins = (valuePlatform: Platforms) => {
        this.setPlatform(valuePlatform);
        this.setCurrentTab(1);
    }
    setVerifyOrder = () => {
        this.setCurrentTab(2);
    }
    setConfirmOrder = () => {
        const { isAuth, beginOrderAndGoToRegistry } = this.props;
        if (!isAuth) {
            beginOrderAndGoToRegistry();
        }
        else {
            this.setCurrentTab(3);
        }
    }

    render() {
        const classes = styles;
        const { currentTab } = this.state;
        const { countCoins, platform } = this.props;

        const myTabs = this.getMyTabList();

        const myTabItems = myTabs.map((tab) => {
            return <Tab key={tab.index} label={tab.label} {...a11yProps(tab.index)} disabled={tab.disabled}/>;
        });
        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Tabs value={currentTab} onChange={this.handleChange} aria-label="simple tabs example">
                        {myTabItems}
                    </Tabs>
                </AppBar>
                <TabPanel value={currentTab} index={0}>
                    Выберите Вашу платформу
                    <br/>
                    <Button
                        color="primary"
                        onClick={this.setPlayStation}
                    >
                        PlayStation 4
                    </Button>
                    <Button
                        color="primary"
                        onClick={this.setXbox}
                    >
                        Xbox One
                    </Button>
                </TabPanel>
                <TabPanel value={currentTab} index={1}>
                    <CoinsCountConnected/>

                    <Button
                        color="primary"
                        onClick={this.setVerifyOrder}
                    >
                        Далее
                    </Button>
                </TabPanel>
                <TabPanel value={currentTab} index={2}>
                    Проверьте заказ и нажмите "Далее" для перехода к оплате.
                    <br/>
                    После оплаты вы сможете перейти к доставке монет.
                    <br/>
                    Информация о заказе:
                    <br/>
                    Количество монет: {countCoins}
                    <br/>
                    Платформа: {platform}

                    <br/>

                    <Button
                        color="primary"
                        onClick={this.setConfirmOrder}
                    >
                        Далее
                    </Button>
                </TabPanel>
                <TabPanel value={currentTab} index={3}>
                    Тут мы открываем форму логина и переводим на оплату счёта.
                </TabPanel>
            </div>
        );
    }
}
