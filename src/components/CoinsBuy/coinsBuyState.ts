import { User } from "../../ducks/users/users";


// Необходимые состояния для работы компонента
export interface coinsBuyState {
    rateBuy: number, // Курс покупки монет
    user?: User | any, // Пользователь, который оформляет заказ, изначально он может быть не залогинен
    minCoinsAmount: number, // Минимум монет, которое можно купить
    maxCoinsAmount: number, // Максимум монет, которое можно купить

};

// Заказ пользователя
export interface order {
    platform: string,
    countCoinsToBuy: number, // Количество монет для покупки

}
