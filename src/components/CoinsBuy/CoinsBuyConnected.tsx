import {connect, MapDispatchToProps, MapStateToProps} from "react-redux";
import {CoinsBuy, CoinsBuyDispatchProps, CoinsBuyStateProps} from "./CoinsBuy";
import { State } from "../../ducks/rootReducer";
import {
    selectCountCoins,
    selectIsAuthenticated,
    selectIsOrderInProgress,
    selectPlatform
} from "../../selectors/selectors";
import {beginOrderAndGoToRegistry, setPlatform} from "../../ducks/order/order";

const dispatchProps: MapDispatchToProps<CoinsBuyDispatchProps, {}> = {
    setPlatform: setPlatform,
    beginOrderAndGoToRegistry: beginOrderAndGoToRegistry,
};

const mapStateToProps: MapStateToProps<CoinsBuyStateProps, {}, State> = state => {
    return {
        countCoins: selectCountCoins(state),
        platform: selectPlatform(state),
        isAuth: selectIsAuthenticated(state),
        isOrderInProgress: selectIsOrderInProgress(state),
    }
}

export const CoinsBuyConnected = connect(
    mapStateToProps,
    dispatchProps
)(CoinsBuy);
