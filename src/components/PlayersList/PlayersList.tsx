import * as React from "react";
import MaterialTable, { Column, QueryResult } from "material-table";
import { tableIcons } from "./tableIcons";
import Edit from "@material-ui/icons/Edit";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

import { Player } from "../../ducks/players/players";

import styles from "./PlayersList.module.scss";


export interface PlayersListStateProps {
    players: Player[];
}

export type PlayersListProps = PlayersListStateProps;

const columns: Column<Player>[] = [
    { title: "Name", field: "name" },
    { title: "Platform", field: "platform" },
    { title: "Card Type", field: "cardType" },
    { title: "Current Price", field: "currentPrice" },
    { title: "Max Price", field: "maxPrice" },
];

export class PlayersList extends React.PureComponent<PlayersListProps> {
    public render() {
        return (
            <div className={styles.container}>
                <MaterialTable
                    title="Players"
                    icons={tableIcons}
                    columns={columns}
                    data={this.getUsersData}
                    actions={[
                        {
                            tooltip: "Edit",
                            icon: () => <Edit />,
                            onClick: (e, player) => this.onEdit(player as Player),
                        },
                        {
                            tooltip: "Remove",
                            icon: () => <DeleteOutline />,
                            onClick: (e, player) => this.onDelete((player as Player).id),
                        },
                    ]}
                />
            </div>
        );
    }

    onEdit = (player: Player) => {

    }

    onDelete = (id: number) => {

    }

    private getUsersData = (): Promise<QueryResult<Player>> => {
        return Promise.resolve({
            data: this.props.players,
            totalCount: 100,
            page: 0,
        });
    };

    private onChangePage = (page: number) => {
        // this.props.onQuery({ page: page });
    };
}
