import {storiesOf} from "@storybook/react";
import {PlayersList} from "./PlayersList";
import React from "react";
import {Platforms} from "../../ducks/platform/platform";

storiesOf("PlayersList", module).add("default", () => {
    const players = [
        {
            id: 1,
            platform: Platforms.PLATFORM_PLAY_STATION_4,
            name: "Ronaldo",
            cardType: 1,
            currentPrice: 50000,
            maxPrice: 200000,
        },
        {
            id: 2,
            platform: Platforms.PLATFORM_XBOX_ONE,
            name: "Zidan",
            cardType: 2,
            currentPrice: 40000,
            maxPrice: 160000,
        },
    ];

    return <PlayersList players={players} />;
});
