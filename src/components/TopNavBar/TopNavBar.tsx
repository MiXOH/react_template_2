import React from "react";
import { Button, AppBar, Toolbar } from "@material-ui/core";
import Popover from '@material-ui/core/Popover';

import styles from "./TopNavBar.module.scss";
import { LoginFormConnected } from "../LoginForm/LoginFormConnected";
import { RegistryFormConnected } from "../RegistryForm/RegistryFormConnected";
import { UserCoinsCountConnected } from "../UserCoinsCount/UserCoinsCountConnected";
import { UserMoneyCountConnected } from "../UserMoneyCount/UserMoneyCountConnected";

export interface TopNavBarStateProps {
    isAuthenticated: boolean;
}

export interface TopNavBarDispatchProps {
    logout: () => void;
    redirectToCreateUser: () => void;
}

interface TopNavBarState {
    anchorEl: HTMLButtonElement | null;
    anchorElReg: HTMLButtonElement | null;
}

export type TopNavBarProps = TopNavBarStateProps & TopNavBarDispatchProps;

export class TopNavBar extends React.PureComponent<TopNavBarProps, TopNavBarState> {
    constructor(props: TopNavBarProps) {
        super(props);

        this.state = {
            anchorEl: null,
            anchorElReg: null,
        }
    }

    handleClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: null });
    }

    handleClickReg = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        this.setState({ anchorElReg: event.currentTarget });
    }

    handleCloseReg = () => {
        this.setState({ anchorElReg: null });
    }

    public render() {
        const { isAuthenticated, logout, redirectToCreateUser } = this.props;
        const { anchorEl, anchorElReg } = this.state;

        const open = Boolean(anchorEl);
        const id = open ? 'simple-popover' : undefined;

        const openReg = Boolean(anchorElReg);
        const idReg = openReg ? 'simple-popover' : undefined;

        return (
            <AppBar position="static">
                <Toolbar className={styles.toolbarContainer}>
                    <div>
                        <Button color="inherit" href={"/"}>
                            Main
                        </Button>
                        <Button color="inherit" href={"/buycoins"}>
                            Buy Coins
                        </Button>
                        <Button color="inherit" href={"/sellcoins"}>
                            Sell Coins
                        </Button>
                        {isAuthenticated && (
                            <Button color="inherit" href={"/users"}>
                                Users
                            </Button>
                        )}
                    </div>

                    {isAuthenticated && (
                        <div>
                            <UserCoinsCountConnected />
                            <UserMoneyCountConnected />
                            <Button color="inherit" onClick={redirectToCreateUser}>
                                Create user
                            </Button>
                            <Button color="inherit" onClick={logout}>
                                Logout
                            </Button>
                        </div>
                    )}
                    {!isAuthenticated && (
                        <div>
                            <Button aria-describedby={id} color="inherit" onClick={this.handleClick}>
                                Login
                            </Button>
                            <Button aria-describedby={idReg} color="inherit" onClick={this.handleClickReg}>
                                Registry
                            </Button>
                        </div>
                    )}
                </Toolbar>
                <Popover
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={this.handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                >
                    <LoginFormConnected/>
                </Popover>
                <Popover
                    id={idReg}
                    open={openReg}
                    anchorEl={anchorElReg}
                    onClose={this.handleCloseReg}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                >
                    <RegistryFormConnected/>
                </Popover>
            </AppBar>
        );
    }
}
