import {storiesOf} from "@storybook/react";
import {OrdersList} from "./OrdersList";
import React from "react";
import {Platforms} from "../../ducks/platform/platform";

storiesOf("OrdersList", module).add("default", () => {
    const orders = [
        {
            id: 1,
            platform: Platforms.PLATFORM_PLAY_STATION_4,
            name: "Ronaldo",
            time: "00:34:52",
            coins: 150000,
            money: 125,
        },
        {
            id: 2,
            platform: Platforms.PLATFORM_XBOX_ONE,
            name: "Zidan",
            time: "00:16:24",
            coins: 130000,
            money: 112,
        },
    ];

    return <OrdersList orders={orders} />;
});
