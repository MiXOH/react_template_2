import * as React from "react";
import MaterialTable, { Column, QueryResult } from "material-table";
import { tableIcons } from "./tableIcons";
import Edit from "@material-ui/icons/Edit";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

import { Button } from "@material-ui/core";

import { Order } from "../../ducks/order/order";

import styles from "./OrdersList.module.scss";

export interface OrdersListStateProps {
    orders: Order[];
}

export type OrdersListProps = OrdersListStateProps;

const columns: Column<Order>[] = [
    { title: "Platform", field: "platform" },
    { title: "Name", field: "name" },
    { title: "Time", field: "time", type: "time" },
    { title: "Coins", field: "coins" },
    { title: "Money", field: "money", type: "currency" },
];

export class OrdersList extends React.PureComponent<OrdersListProps> {
    public render() {
        const options = {
            header: false,
            paging: false,
            search: false,
            actionsColumnIndex: 5,
        }
        return (
            <div className={styles.container}>
                <MaterialTable
                    title="Orders"
                    icons={tableIcons}
                    columns={columns}
                    data={this.getOrdersData}
                    actions={[
                        {
                            tooltip: "Choose",
                            icon: () =>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    fullWidth={false}
                                    size="large"
                                >
                                    Choose
                                </Button>,
                            onClick: (e, order) => this.onEdit(order as Order),
                        },
                    ]}
                    options={options}
                />
            </div>
        );
    }

    onEdit = (order: Order) => {

    }

    onDelete = (id: number) => {

    }

    private getOrdersData = (): Promise<QueryResult<Order>> => {
        return Promise.resolve({
            data: this.props.orders,
            totalCount: 100,
            page: 0,
        });
    };

    private onChangePage = (page: number) => {
        // this.props.onQuery({ page: page });
    };
}
