import {connect, MapDispatchToProps, MapStateToProps} from "react-redux";
import { OrderBuy, OrderBuyDispatchProps, OrderBuyStateProps } from "./OrderBuy";
import { setCountOwnCoins } from "../../ducks/order/order";
import { State } from "../../ducks/rootReducer";
import { selectCountDeliveredCoins, selectCountCoins } from "../../selectors/selectors";

const dispatchProps: MapDispatchToProps<OrderBuyDispatchProps, {}> = {
    setCountOwnCoins: setCountOwnCoins,
};

const mapStateToProps: MapStateToProps<OrderBuyStateProps, {}, State> = state => {
    return {
        countDeliveredCoins: selectCountDeliveredCoins(state),
        countCoins: selectCountCoins(state),
    }
}

export const OrderBuyConnected = connect(
    mapStateToProps,
    dispatchProps,
)(OrderBuy);
