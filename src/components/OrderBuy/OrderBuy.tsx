import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';

import styles from "./OrderBuy.module.scss";

export interface OrderBuyDispatchProps {
    setCountOwnCoins: (countCoins: number) => void
}

export interface OrderBuyStateProps {
    countDeliveredCoins: number,
    countCoins: number,
}

export type OrderBuyProps = OrderBuyStateProps & OrderBuyDispatchProps;

export class OrderBuy extends React.PureComponent<OrderBuyProps> {
    constructor(props: OrderBuyProps) {
        super(props);
    }

    setValue = (newValue: number | number[]) => {
        const { setCountOwnCoins } = this.props;
        setCountOwnCoins(Number(newValue));
    }

    changeValue = (newValue: number | number[]) => {
        this.setValue(newValue);
    }

    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const val = event.target.value === '' ? '' : Number(event.target.value);
        this.changeValue(Number(val));
    };

    public render() {
        const classes = styles;

        const { countDeliveredCoins, countCoins } = this.props;

        return (
            <div className={classes.root}>
                <Typography id="input-slider" gutterBottom>
                    Доставлено {countDeliveredCoins} из {countCoins}
                </Typography>
                <Grid container spacing={2} alignItems="center">
                    <Grid item xs>
                        <Slider
                            disabled={true}
                            value={typeof countDeliveredCoins === 'number' ? countDeliveredCoins : 0}
                            aria-labelledby="input-slider"
                            max={countCoins}
                            min={0}
                        />
                    </Grid>
                    <Grid item>
                        Сколько у вас есть монет
                        <Input
                            className={classes.input}
                            margin="dense"
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }
}