import { storiesOf } from "@storybook/react";
import { RecoverPassword } from "./RecoverPassword";
import React from "react";
import { action } from "@storybook/addon-actions";

storiesOf("RecoverPassword", module).add("default", () => {
    return (
        <div
            style={{
                margin: 40,
                maxWidth: 450,
            }}
        >
            <RecoverPassword/>
        </div>
    );
});
