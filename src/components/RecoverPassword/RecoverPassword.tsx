import * as React from "react";
import { Button, TextField } from "@material-ui/core";

import styles from "./RecoverPassword.module.scss";

export class RecoverPassword extends React.PureComponent {
    public render() {
        return (
            <div className={styles.recoverPasswordContainer}>
                <TextField
                    className={styles.input}
                    label="Email"
                    variant="outlined"
                    fullWidth
                />
                <Button
                    className={styles.recoverPasswordContainer}
                    variant="contained"
                    color="primary"
                    fullWidth={false}
                    size="large"
                >
                    Recover password
                </Button>
            </div>
        );
    }
}
