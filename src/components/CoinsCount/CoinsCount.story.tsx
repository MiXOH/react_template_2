import { storiesOf } from "@storybook/react";
import { CoinsCount } from "./CoinsCount";
import React from "react";
import { action } from "@storybook/addon-actions";

storiesOf("CoinsCount", module).add("default", () => {
    const rateBuy = 250;
    const countCoins = 50000;
    const setCountCoinsToBuy = action("setCountCoinsToBuy");

    return (
        <div
            style={{
                margin: 40,
                maxWidth: 450,
            }}
        >
            <CoinsCount setCountCoinsToBuy={setCountCoinsToBuy} rateBuy={rateBuy} countCoins={countCoins} />
        </div>
    );
});
