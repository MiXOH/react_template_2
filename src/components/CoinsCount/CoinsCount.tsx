import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';

import styles from "./CoinsCount.module.scss";

export interface CoinsCountDispatchProps {
    setCountCoinsToBuy: (countCoins: number) => void
}

export interface CoinsCountStateProps {
    rateBuy: number,
    countCoins: number,
}

export type CoinsCountProps = CoinsCountStateProps & CoinsCountDispatchProps;

export class CoinsCount extends React.PureComponent<CoinsCountProps> {
    constructor(props: CoinsCountProps) {
        super(props);
    }

    setValue = (newValue: number | number[]) => {
        const { setCountCoinsToBuy } = this.props;
        setCountCoinsToBuy(Number(newValue));
    }

    setValueMoney = (newValue: number | number[]) => {
        const { rateBuy } = this.props;
        this.setValue(Number(newValue) * rateBuy);
    }

    changeValue = (newValue: number | number[]) => {
        this.setValue(newValue);
    }

    changeMoneyValue = (newValue: number | number[]) => {
        this.setValueMoney(newValue);
    }

    handleSliderChange = (event: any, newValue: number | number[]) => {
        this.changeValue(newValue);
    };

    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const val = event.target.value === '' ? '' : Number(event.target.value);
        this.changeValue(Number(val));
    };

    handleInputMoneyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const val = event.target.value === '' ? '' : Number(event.target.value);
        this.changeMoneyValue(Number(val));
    };

    handleBlur = () => {
        const { countCoins } = this.props;
        if (countCoins < 1000) {
            this.changeValue(1000);
        } else if (countCoins > 1000000) {
            this.changeValue(1000000);
        }
    };

    public render() {
        const classes = styles;

        const { countCoins, rateBuy } = this.props;
        const valueMoney = Number(countCoins) / rateBuy;

        return (
            <div className={classes.root}>
                <Typography id="input-slider" gutterBottom>
                    Количество монет
                </Typography>
                <Grid container spacing={2} alignItems="center">
                    <Grid item xs>
                        Монет
                        <Slider
                            value={typeof countCoins === 'number' ? countCoins : 1000}
                            onChange={this.handleSliderChange}
                            aria-labelledby="input-slider"
                            max={1000000}
                            min={1000}
                        />
                    </Grid>
                    <Grid item>
                        Монет
                        <Input
                            className={classes.input}
                            value={countCoins}
                            margin="dense"
                            onChange={this.handleInputChange}
                            onBlur={this.handleBlur}
                            inputProps={{
                                step: 1000,
                                min: 1000,
                                max: 1000000,
                                type: 'number',
                                'aria-labelledby': 'input-slider',
                            }}
                        />
                    </Grid>
                    <Grid item>
                        Денег
                        <Input
                            className={classes.input}
                            value={valueMoney}
                            margin="dense"
                            onChange={this.handleInputMoneyChange}
                            onBlur={this.handleBlur}
                            inputProps={{
                                step: 1,
                                min: 0,
                                max: 4000,
                                type: 'number',
                                'aria-labelledby': 'input-slider',
                            }}
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }
}