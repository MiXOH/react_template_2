import {connect, MapDispatchToProps, MapStateToProps} from "react-redux";
import { CoinsCount, CoinsCountDispatchProps, CoinsCountStateProps } from "./CoinsCount";
import { setCountCoinsToBuy } from "../../ducks/order/order";
import { State } from "../../ducks/rootReducer";
import { selectCountCoins, selectRateBuy } from "../../selectors/selectors";

const dispatchProps: MapDispatchToProps<CoinsCountDispatchProps, {}> = {
    setCountCoinsToBuy: setCountCoinsToBuy,
};

const mapStateToProps: MapStateToProps<CoinsCountStateProps, {}, State> = state => {
    return {
        rateBuy: selectRateBuy(state),
        countCoins: selectCountCoins(state),
    }
}

export const CoinsCountConnected = connect(
    mapStateToProps,
    dispatchProps,
)(CoinsCount);
