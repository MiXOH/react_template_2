import * as React from "react";
import { Button, TextField } from "@material-ui/core";

import styles from "./LoginForm.module.scss";

export interface LoginFormDispatchProps {
    authentication: (email: string, password: string) => void;
}

export type LoginFormProps = LoginFormDispatchProps;

export interface LoginFormState {
    email: string;
    password: string;
    emailError: boolean;
    passwordError: boolean;
    emailErrorText: string;
    passwordErrorText: string;
}

export class LoginForm extends React.PureComponent<LoginFormProps, LoginFormState> {
    state = {
        email: "",
        password: "",
        emailError: false,
        passwordError: false,
        emailErrorText: "",
        passwordErrorText: "",
    };

    public render() {
        const { emailError, passwordError, emailErrorText, passwordErrorText } = this.state;
        return (
            <div className={styles.loginContainer}>
                <TextField
                    required
                    error={emailError}
                    helperText={emailErrorText}
                    className={styles.input}
                    label="Email"
                    variant="outlined"
                    fullWidth
                    onChange={this.handleChange("email")}
                />
                <TextField
                    required
                    error={passwordError}
                    helperText={passwordErrorText}
                    className={styles.input}
                    label="Пароль"
                    type="password"
                    variant="outlined"
                    onChange={this.handleChange("password")}
                    fullWidth
                />
                <Button
                    className={styles.loginContainer}
                    variant="contained"
                    color="primary"
                    fullWidth={false}
                    size="large"
                    onClick={this.authentication}
                >
                    Войти
                </Button>
            </div>
        );
    }

    private handleChange = (name: keyof LoginFormState) => (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, [name]: event.target.value || event.target.name });
    };

    private wrongEmail(isWrong: boolean) {
        if (isWrong) {
            this.setState({ emailError: true, emailErrorText: "Поле обязательно для заполнения" });
        }
        else {
            this.setState({ emailError: false, emailErrorText: "" });
        }
    }

    private wrongPassword(isWrong: boolean) {
        if (isWrong) {
            this.setState({
                passwordError: true,
                passwordErrorText: "Поле обязательно для заполнения",
            });
        }
        else {
            this.setState({
                passwordError: false,
                passwordErrorText: "",
            });
        }
    }

    private authentication = () => {
        const { authentication } = this.props;
        const { email, password } = this.state;
        let isValid = true;

        if (!email) {
            isValid = false;
            this.wrongEmail(true);
        }
        else {
            this.wrongEmail(false);
        }

        if (!password) {
            isValid = false;
            this.wrongPassword(true);
        }
        else {
            this.wrongPassword(false);
        }

        if (isValid) {
            authentication(email, password);
        }
    };
}
