import { storiesOf } from "@storybook/react";
import { RegistryForm } from "./RegistryForm";
import React from "react";
import { action } from "@storybook/addon-actions";

storiesOf("RegistryForm", module).add("default", () => {
    return (
        <div
            style={{
                margin: 40,
                maxWidth: 450,
            }}
        >
            <RegistryForm createNewUser={action("createNewUser")} />
        </div>
    );
});
