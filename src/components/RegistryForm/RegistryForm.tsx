import * as React from "react";
import { Button, TextField } from "@material-ui/core";

import styles from "./RegistryForm.module.scss";

import { isRegisteredUser, NewUser, User, UserRole } from "../../ducks/users/users";


export interface RegistryFormDispatchProps {
    createNewUser: (user: NewUser) => void;
}

export type RegistryFormProps = RegistryFormDispatchProps;

export interface RegistryFormState {
    email: string;
    password: string;
    passwordRepeat: string;
    emailError: boolean;
    passwordError: boolean;
    passwordRepeatError: boolean;
    emailErrorText: string;
    passwordErrorText: string;
    passwordRepeatErrorText: string;
}

export class RegistryForm extends React.PureComponent<RegistryFormProps, RegistryFormState> {
    state = {
        email: "",
        password: "",
        passwordRepeat: "",
        emailError: false,
        passwordError: false,
        passwordRepeatError: false,
        emailErrorText: "",
        passwordErrorText: "",
        passwordRepeatErrorText: "",
    };

    public render() {
        const { emailError, passwordError, passwordRepeatError, emailErrorText, passwordErrorText, passwordRepeatErrorText } = this.state;
        return (
            <div className={styles.registryContainer}>
                <TextField
                    required
                    error={emailError}
                    helperText={emailErrorText}
                    className={styles.input}
                    label="Email"
                    variant="outlined"
                    fullWidth
                    onChange={this.handleChange("email")}
                />
                <TextField
                    required
                    error={passwordError}
                    helperText={passwordErrorText}
                    id="standard-password-input"
                    label="Password"
                    className={styles.input}
                    type="password"
                    variant="outlined"
                    fullWidth
                    onChange={this.handleChange("password")}
                />
                <TextField
                    required
                    error={passwordRepeatError}
                    helperText={passwordRepeatErrorText}
                    id="standard-password-input"
                    label="Password repeat"
                    className={styles.input}
                    type="password"
                    variant="outlined"
                    fullWidth
                    onChange={this.handleChange("passwordRepeat")}
                />
                <Button
                    className={styles.registryContainer}
                    variant="contained"
                    color="primary"
                    fullWidth={false}
                    size="large"
                    onClick={this.createNewUser}
                >
                    Registry
                </Button>
            </div>
        );
    }

    private handleChange = (name: keyof RegistryFormState) => (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, [name]: event.target.value || event.target.name });
    };

    private getUser = (): User => {
        const { email, password } = this.state;

        const user = {
            id: '0',
            role: UserRole.ROLE_CLIENT,
            email: this.state.email,
            password: this.state.password,
        }

        return user as User;
    };

    private wrongEmail(isWrong: boolean) {
        if (isWrong) {
            this.setState({ emailError: true, emailErrorText: "Поле заполнено не верно" });
        }
        else {
            this.setState({ emailError: false, emailErrorText: "" });
        }
    }

    private wrongPassword(isWrong: boolean) {
        if (isWrong) {
            this.setState({
                passwordError: true,
                passwordErrorText: "Поле заполнено не верно",
                passwordRepeatError: true,
                passwordRepeatErrorText: "Поле заполнено не верно"
            });
        }
        else {
            this.setState({
                passwordError: false,
                passwordErrorText: "",
                passwordRepeatError: false,
                passwordRepeatErrorText: ""
            });
        }
    }

    private createNewUser = () => {
        // Проводим верефикацию формы
        const { email, password, passwordRepeat } = this.state;
        let isValid = true;

        // Валидация электронного адреса
        if ((email == "") || (email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email))) {
            isValid = false;
            this.wrongEmail(true);
        }
        else {
            this.wrongEmail(false);
        }

        // Вылидация пароля
        if (password == "" || passwordRepeat == "" || password != passwordRepeat) {
            isValid = false;
            this.wrongPassword(true);
        }
        else {
            this.wrongPassword(false);
        }

        if (isValid) {
            // TODO: Необходимо вывести добавление пользователя из под авторизации
            //this.props.createNewUser(this.getUser())
        }
    };
}
