import { connect, MapDispatchToProps } from "react-redux";
import { RegistryForm, RegistryFormDispatchProps } from "./RegistryForm";
import { createNewUser } from "../../ducks/users/users";

const dispatchProps: MapDispatchToProps<RegistryFormDispatchProps, {}> = {
    createNewUser: createNewUser,
};

export const RegistryFormConnected = connect(
    undefined,
    dispatchProps
)(RegistryForm);
