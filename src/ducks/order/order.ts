import { Thunk, TypedAction } from "../types";
import {Platforms} from "../platform/platform";
import {push} from "connected-react-router";
export const SET_COUNT_COINS_TO_BUY = "ORDER/SET_COUNT_COINS_TO_BUY";
export const SET_PLATFORM = "ORDER/SET_PLATFORM";
export const SET_IS_ORDER_IN_PROGRESS = "ORDER/SET_IS_ORDER_IN_PROGRESS";
export const SET_COUNT_OWN_COINS = "ORDER/SET_COUNT_OWN_COINS";

export type OrderActions = TypedAction<typeof SET_COUNT_COINS_TO_BUY, OrderState> | TypedAction<typeof SET_PLATFORM, OrderState>
    | TypedAction<typeof SET_IS_ORDER_IN_PROGRESS, OrderState> | TypedAction<typeof SET_COUNT_OWN_COINS, OrderState>;

export interface Order {
    id: number,
    platform: Platforms,
    name: string,
    time: string,
    coins: number,
    money: number,
    countOwnCoins?: number,
}

export interface OrderState {
    platform: Platforms | undefined,
    countCoins: number,
    isOrderInProgress: boolean,
    countOwnCoins: number,
    countDeliveredCoins: number,
};

const initialState : OrderState = {
    platform: undefined,
    countCoins: 0,
    isOrderInProgress: false,
    countOwnCoins: 0,
    countDeliveredCoins: 0,
}

export function beginOrderAndGoToRegistry(): Thunk {
    return (dispatch, getState) => {
        dispatch({
            type: SET_IS_ORDER_IN_PROGRESS,
            payload: {isOrderInProgress: true},
        });

        // Сохраняем в localStorage, чтобы использовать в будущем
        const orderState = getState().order;
        window.localStorage.setItem("orderState", JSON.stringify(orderState));

        dispatch(push('/registry'));
    }
}

export function setCountCoinsToBuy(value: number) : Thunk {
    return dispatch => {
        dispatch({
            type: SET_COUNT_COINS_TO_BUY,
            payload: {countCoins: value},
        });
    }
}

export function setPlatform(value: string) : Thunk {
    return dispatch => {
        dispatch({
            type: SET_PLATFORM,
            payload: {platform: value},
        });
    }
}

export function setCountOwnCoins(value: number) : Thunk {
    return dispatch => {
        dispatch({
            type: SET_COUNT_OWN_COINS,
            payload: {countOwnCoins: value},
        });
    }
}

export function orderReducer(state: OrderState = initialState, action: OrderActions) : OrderState {
    switch (action.type) {
        case SET_COUNT_COINS_TO_BUY:
        case SET_PLATFORM:
        case SET_IS_ORDER_IN_PROGRESS:
        case SET_COUNT_OWN_COINS:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}