import {Platforms} from "../platform/platform";


export interface Player {
    id: number,
    platform: Platforms,
    name: string,
    cardType: number, // Пока не ясно, сущности тип карточки пока нет
    currentPrice: number,
    maxPrice: number,
}