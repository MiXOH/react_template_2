import * as React from "react";
import Grid from '@material-ui/core/Grid';
import { Button } from "@material-ui/core";

import styles from "./MainPage.module.scss";

export class MainPage extends React.PureComponent {
    public render() {
        return (
            <Grid container className={styles.root} spacing={10}>
                <Grid item xs={12}>
                    <Grid container className={styles.control} justify="space-evenly" direction="row" alignItems="center" spacing={10}>
                        <Grid item>
                            <Button
                                size="large"
                                color="primary"
                                variant="contained"
                                href={"/buycoins"}
                            >
                                Купить монеты
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button
                                size="large"
                                color="primary"
                                variant="contained"
                                href={"/sellcoins"}
                            >
                                Продать монеты
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}





/*import {PlayersList} from "../../components/PlayersList/PlayersList";
import {OrdersList} from "../../components/OrdersList/OrdersList";
import {Platforms} from "../../ducks/platform/platform";

export class MainPage extends React.PureComponent {
    public render() {
        const listPlayers = [
            {
                id: 1,
                platform: Platforms.PLATFORM_PLAY_STATION_4,
                name: "Ronaldo",
                cardType: 1,
                currentPrice: 25000,
                maxPrice: 100000,
            },
            {
                id: 2,
                platform: Platforms.PLATFORM_XBOX_ONE,
                name: "Zidan",
                cardType: 2,
                currentPrice: 15000,
                maxPrice: 60000,
            },
            {
                id: 3,
                platform: Platforms.PLATFORM_PLAY_STATION_4,
                name: "Kokorin",
                cardType: 3,
                currentPrice: 5000,
                maxPrice: 30000,
            }
        ];

        const listOrders = [
            {
                id: 1,
                platform: Platforms.PLATFORM_PLAY_STATION_4,
                name: "Ronaldo",
                time: "00:04:56",
                coins: 100000,
                money: 45.5,
            },
            {
                id: 2,
                platform: Platforms.PLATFORM_XBOX_ONE,
                name: "Zidan",
                time: "00:26:18",
                coins: 80000,
                money: 30.5,
            },
            {
                id: 3,
                platform: Platforms.PLATFORM_PLAY_STATION_4,
                name: "Kokorin",
                time: "00:46:25",
                coins: 20000,
                money: 12,
            }
        ];

        return (
            <div>
                <PlayersList players={listPlayers}/>
                <br />
                <br />
                <OrdersList orders={listOrders}/>
            </div>
        );
    }
}*/