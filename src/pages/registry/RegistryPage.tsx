import * as React from "react";

import styles from "./RegistryPage.module.scss";
import { RegistryFormConnected } from "../../components/RegistryForm/RegistryFormConnected";
import {Button} from "@material-ui/core";

export class RegistryPage extends React.PureComponent {
    public render() {
        return (
            <div className={styles.registryPageContainer}>
                <div className={styles.registryForm}>
                    <RegistryFormConnected />
                    <br /><br />
                    Have a login?
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Button
                        className={styles.buttonContainer}
                        variant="contained"
                        color="primary"
                        fullWidth={false}
                        size="large"
                        href={"/login"}
                    >
                        Login
                    </Button>
                </div>
            </div>
        );
    }
}
