import * as React from "react";
import { RecoverPassword } from "../../components/RecoverPassword/RecoverPassword";
import styles from "./RecoverPasswordPage.module.scss";

export class RecoverPasswordPage extends React.PureComponent {
    public render() {
        return (
            <div className={styles.recoverPasswordPageContainer}>
                <div className={styles.recoverPassword}>
                    <RecoverPassword />
                </div>
            </div>
        );
    }
}
