import * as React from "react";

import { Button } from "@material-ui/core";

import { LoginFormConnected } from "../../components/LoginForm/LoginFormConnected";
import styles from "./LoginPage.module.scss";

export class LoginPage extends React.PureComponent {
    public render() {
        return (
            <div className={styles.loginPageContainer}>
                <div className={styles.loginForm}>
                    <LoginFormConnected />

                    <br /><br />
                    <Button
                        className={styles.buttonContainer}
                        variant="contained"
                        color="primary"
                        fullWidth={false}
                        size="large"
                        href={"/recover"}
                    >
                        Recover password
                    </Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Button
                        className={styles.buttonContainer}
                        variant="contained"
                        color="primary"
                        fullWidth={false}
                        size="large"
                        href={"/registry"}
                    >
                        Registry
                    </Button>
                </div>
            </div>
        );
    }
}
