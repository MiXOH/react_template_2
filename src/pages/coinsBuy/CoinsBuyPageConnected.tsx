import {connect, MapDispatchToProps, MapStateToProps} from "react-redux";
import { CoinsBuyPage, CoinsBuyPageStateProps } from "./CoinsBuyPage";
import { State } from "../../ducks/rootReducer";
import { selectIsOrderInProgress, selectCountCoins, selectCountDeliveredCoins } from "../../selectors/selectors";

const mapStateToProps: MapStateToProps<CoinsBuyPageStateProps, {}, State> = state => {
    return {
        isOrderInProgress: selectIsOrderInProgress(state),
        countCoins: selectCountCoins(state),
        countDeliveredCoins: selectCountDeliveredCoins(state),
    }
}

export const CoinsBuyPageConnected = connect(
    mapStateToProps,
    null,
)(CoinsBuyPage);
