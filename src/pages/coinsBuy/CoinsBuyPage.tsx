import * as React from "react";
import { CoinsBuyConnected } from "../../components/CoinsBuy/CoinsBuyConnected";
import { OrderBuy } from "../../components/OrderBuy/OrderBuy";
import { TopNavBarConnected } from "../../components/TopNavBar/TopNavBarConnected";
import { BottomNavBar, BottomNavBarItems } from "../../components/BottomNavBar/BottomNavBar";

export interface CoinsBuyPageStateProps {
    isOrderInProgress: boolean,
    countCoins: number,
    countDeliveredCoins: number,
}

export type CoinsBuyPageProps = CoinsBuyPageStateProps;

export class CoinsBuyPage extends React.PureComponent<CoinsBuyPageProps> {
    constructor(props: CoinsBuyPageProps) {
        super(props);
    }

    setCoins = (val: number) => {

    }

    public render() {
        const { isOrderInProgress, countCoins, countDeliveredCoins } = this.props;

        const bottomNavBarItem = {
            title: "Sell Coins",
            link: "/sellcoins",
        }

        let orderComponent = (<CoinsBuyConnected/>);
        if (isOrderInProgress) {
            orderComponent = (<OrderBuy countDeliveredCoins={countDeliveredCoins} countCoins={countCoins} setCountOwnCoins={this.setCoins}/>)
        }

        return (
            <div>
                <TopNavBarConnected />
                {orderComponent}
                <BottomNavBar item={bottomNavBarItem} />
            </div>
        );
    }
}
