import * as React from "react";

import { OrdersList } from "../../components/OrdersList/OrdersList";
import {Platforms} from "../../ducks/platform/platform";
import {TopNavBarConnected} from "../../components/TopNavBar/TopNavBarConnected";
import {BottomNavBar} from "../../components/BottomNavBar/BottomNavBar";

export class CoinsSellPage extends React.PureComponent {
    public render() {
        const listOrders = [
            {
                id: 1,
                platform: Platforms.PLATFORM_PLAY_STATION_4,
                name: "Ronaldo",
                time: "00:04:56",
                coins: 100000,
                money: 45.5,
            },
            {
                id: 2,
                platform: Platforms.PLATFORM_XBOX_ONE,
                name: "Zidan",
                time: "00:26:18",
                coins: 80000,
                money: 30.5,
            },
            {
                id: 3,
                platform: Platforms.PLATFORM_PLAY_STATION_4,
                name: "Kokorin",
                time: "00:46:25",
                coins: 20000,
                money: 12,
            }
        ];

        const bottomNavBarItem = {
            title: "Buy Coins",
            link: "/buycoins",
        }

        return (
            <div>
                <TopNavBarConnected />
                <OrdersList orders={listOrders}/>
                <BottomNavBar item={bottomNavBarItem} />
            </div>
        );
    }
}
