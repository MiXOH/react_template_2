import React from "react";
import { Provider } from "react-redux";
import { Route } from "react-router-dom";
import { LoginPage } from "../pages/login/LoginPage";
import { MainPage } from "../pages/main/MainPage";
import { RegistryPage } from "../pages/registry/RegistryPage";
import { RecoverPasswordPage } from "../pages/recoverPassword/RecoverPasswordPage";
import { CoinsBuyPageConnected } from "../pages/coinsBuy/CoinsBuyPageConnected";
import { CoinsSellPage } from "../pages/coinsSell/CoinsSellPage";
import thunk from "redux-thunk";
import { applyMiddleware, compose, createStore } from "redux";
import { rootReducer, history } from "../ducks/rootReducer";
import { TopNavBarConnected } from "../components/TopNavBar/TopNavBarConnected";
import { BottomNavBar } from "../components/BottomNavBar/BottomNavBar";
import { PrivateRouteConnected } from "./PrivateRoute";
import { ConnectedRouter, routerMiddleware } from "connected-react-router";
import { UsersPage } from "../pages/users/UsersPage";
import { EditUserPage } from "../pages/editUser/EditUserPage";
import { initApiUtils } from "../api/utils";

const authState = JSON.parse(window.localStorage.getItem("authState") || `{"id": 0}`);
// TODO: Что-то мне не нравится как тут передаются значения по умолчанию, надо переделать
const orderState = JSON.parse(window.localStorage.getItem("orderState") || `{"platform": "undefined", "countCoins": 0, "isOrderInProgress": false, "countOwnCoins": 0, "countDeliveredCoins": 0}`);

export const store = createStore(
    rootReducer,
    { auth: authState, order: orderState },
    compose(applyMiddleware(thunk, routerMiddleware(history))),
);

initApiUtils(store.dispatch, store.getState);

export function App() {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>

                <Route exact path="/" component={MainPage} />
                <Route path="/login" component={LoginPage} />
                <Route path="/registry" component={RegistryPage} />
                <Route path="/recover" component={RecoverPasswordPage} />
                <Route path="/buycoins" component={CoinsBuyPageConnected} />
                <Route path="/sellcoins" component={CoinsSellPage} />
                <PrivateRouteConnected path="/users" exact component={UsersPage} />
                <PrivateRouteConnected path="/users/edit" component={EditUserPage} />

            </ConnectedRouter>
        </Provider>
    );
}
