import { State } from "../ducks/rootReducer";

export function selectIsAuthenticated(state: State) {
    return !!state.auth.id;
}

export function selectActiveUser(state: State) {
    return state.users.activeUser;
}

export function selectUsers(state: State) {
    return state.users.users;
}

export function selectUsersCountCoins(state: State) {
    return (state.users.activeUser) ? state.users.activeUser.countCoins : null;
}

export function selectUsersCountMoney(state: State) {
    return (state.users.activeUser) ? state.users.activeUser.countMoney : null;
}

export function selectRateBuy(state: State) {
    return 250;
}

export function selectCountCoins(state: State) {
    return state.order.countCoins;
}

export function selectPlatform(state: State) {
    return state.order.platform;
}

export function selectIsOrderInProgress(state: State) {
    return state.order.isOrderInProgress;
}

export function selectCountDeliveredCoins(state: State) {
    return state.order.countDeliveredCoins;
}
